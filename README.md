![](./image/circles.png)
un jeu vidéo nommé Circles, fortement inspiré du jeu Super Hexagon de Terry Cavanagh.
L'implémentation doit être faite à l'aide de la bibliothèque SDL2.
le joueur est un triangle bleu. Les seuls déplacements qu'il
peut effectuer sont contraints autour du disque gris qu'on aperçoit au milieu
de la scène. Il peut se déplacer soit dans le sens horaire, soit dans le sens
antihoraire.
Au fur et à mesure que le temps avance, des obstacles, représentés par des arcs
rouges, se déplacent ver le centre de la scène. Le joueur doit éviter ces
obstacles. Si, à n'importe quel moment, le triangle et un arc de cercle sont en
contact, alors le joueur perd et l'application retourne au menu principal (voir
l'image ci-bas).

![](./image/menu.png)
