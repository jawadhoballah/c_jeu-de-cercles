# Travail pratique 3 : Le jeu *Circles*

## Description

Description du projet en quelques phrases.
Mentionner le contexte (cours, sigle, université, etc.).

## Auteurs

- jawad hoballah (Code permanent)
- Prénom et nom (Code permanent)
- Prénom et nom (Code permanent)

## Dépendances

Avant même d'expliquer le fonctionnement de votre programme, décrivez toutes
les dépendances de votre projet, autant au niveau des logiciels (s'il y en a)
que des bibliothèques (par exemple SDL2). Fournissez les liens vers les sites
officiels des dépendances si possible. Ceci permettra à l'utilisateur de savoir
ce qu'il doit installer pour faire tourner votre programme.

## Fonctionnement

Expliquez comment compiler, comment lancer l'application et comment fonctionne
le jeu. Quel est sont but? Quelles touches permettent de naviguer dans le menu?
Quelles touches permettent de déplacer le triangle? N'hésitez pas à inclure des
captures d'écran ici, comme ça, ça fait de la publicité pour votre jeu!

## Plateformes supportées

Indiquez toutes les plateformes sur lesquelles vous avez testé l'application,
avec la version.

## Références

Citez vos sources ici, s'il y a lieu.

## Division des tâches

Donnez ici une liste des tâches de chacun des membres de l'équipe. Utilisez la
syntaxe suivante (les crochets vides indiquent que la tâche n'est pas
complétée, les crochets avec un `X` indique que la tâche est complétée):

* [ ] Gestion du menu (Alice)
* [ ] Affichage de la scène (Bob)
* [ ] Affichage du chronomètre (Carl)
* [ ] Animation des murs (Bob)
* [X] Animation du joueur (Alice)
* [ ] Détection des collisions (Carl)
* [ ] Affichage d'une partie terminée (Bob)
* [X] Gestion de la musique principale (Alice)
* [ ] Gestion des sons lorsqu'on navigue dans le menu (Alice)
* [ ] Gestion de la musique de fin de partie (Carl)

## État du projet

Indiquer si le projet est complété et sans bogue. Sinon, expliquez ce qui
manque ou ce qui ne fonctionne pas.
```
