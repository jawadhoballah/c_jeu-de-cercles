#include "application.h"
#include "sdl2.h"
#include <stdio.h>
#include "SDL.h"

void SDL_LimitFPS(unsigned int limit){
    unsigned int ticks = SDL_GetTicks();
    if(limit <ticks){

    }
    else if(limit > ticks+16){
        SDL_Delay(16);
    }
        else{
            SDL_Delay(limit - ticks);
        }
    }

void SDL_ExitWithError(const char *message){
    SDL_Log("ERROR : %s > %s\n", message,SDL_GetError());
    SDL_Quit();
    exit(EXIT_FAILURE);
}

struct Application *Application_initialize() {
    struct Application *application;
    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) < 0) {
        fprintf(stderr, "SDL failed to initialize: %s\n", SDL_GetError());
        return NULL;
    }
    if (!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1")) {
        fprintf(stderr, "Warning: Linear texture filtering not enabled!");
    }
    application = malloc(sizeof(struct Application));
    application->window = SDL_CreateWindow("Maze",
        SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
        SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
    if (application->window == NULL) {
        fprintf(stderr, "Window could not be created: %s\n", SDL_GetError());
        return NULL;
    }
    application->renderer = SDL_CreateRenderer(application->window, -1,
        SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (application->renderer == NULL) {
        fprintf(stderr, "Renderer could not be created: %s\n", SDL_GetError());
        return NULL;
    }
    int imgFlags = IMG_INIT_PNG;
    if (!(IMG_Init(imgFlags) & imgFlags)) {
        fprintf(stderr, "SDL_image failed to initialize: %s\n", IMG_GetError());
        return NULL;
    }
    application->menu = Menu_initialize(application->renderer);
    if (application->menu == NULL) {
        fprintf(stderr, "Failed to initialize menu: %s\n", IMG_GetError());
        return NULL;
    }
    application->state = APPLICATION_STATE_MENU;
    return application;
}

void Application_run(struct Application *application) {
    SDL_Window *window =NULL;
    SDL_Renderer *renderer1 =NULL;
    unsigned int frame= 0;
    int a =0;


    while (application->state != APPLICATION_STATE_QUIT) {
        switch (application->state) {
            case APPLICATION_STATE_MENU:
                Menu_run(application->menu);
                if (application->menu->state == MENU_QUIT) {
                    application->state = APPLICATION_STATE_QUIT;
                } else if (application->menu->state == MENU_PLAY) {
                    application->state = APPLICATION_STATE_PLAY;
                }
                break;
            case APPLICATION_STATE_PLAY:

            frame = SDL_GetTicks()+16;

            if(SDL_Init(SDL_INIT_VIDEO| SDL_INIT_AUDIO)!= 0){
                SDL_ExitWithError("Initialisation SDL");
            }
                

                window = SDL_CreateWindow("PLAYING",SDL_WINDOWPOS_CENTERED,SDL_WINDOWPOS_CENTERED ,750,580,0x00000040 );

                if(window==NULL){
                    SDL_ExitWithError("impossible dafficher la fenetre");
                    }

                    renderer1 = SDL_CreateRenderer(window,-1,SDL_RENDERER_SOFTWARE);

                    if(renderer1==NULL){
                        SDL_ExitWithError("Creation rendu chouee");
                    }


                    
                    
                    
                    SDL_Surface *image =NULL;
                    SDL_Texture *texture =NULL;
                    
                    image =SDL_LoadBMP(PHOTO2);

                    
                    if(image==NULL){
                        SDL_DestroyRenderer(renderer1);
                        SDL_DestroyWindow(window);
                        SDL_ExitWithError("impossible de chager l'image");
                    }
                    


                    texture= SDL_CreateTextureFromSurface(renderer1,image);
                    SDL_FreeSurface(image);

                    if(texture==NULL){
                        SDL_DestroyRenderer(renderer1);
                        SDL_DestroyWindow(window);
                        SDL_ExitWithError("impossible de chager la texture");
                    }


                    SDL_Rect rectangle;

                    if(SDL_QueryTexture(texture ,NULL,NULL,&rectangle.w,&rectangle.h)!=0){
                        SDL_DestroyRenderer(renderer1);
                        SDL_DestroyWindow(window);
                        SDL_ExitWithError("impossible de charger la texture");

                    }

                    rectangle.x=(800-rectangle.w)/2;
                    rectangle.y=(600- rectangle.h)/2;

                    if(SDL_RenderCopy(renderer1,texture,NULL,&rectangle)!=0){
                        SDL_DestroyRenderer(renderer1);
                        SDL_DestroyWindow(window);
                        SDL_ExitWithError("impossible de afficher la texture");

                    }
                    SDL_SetRenderDrawColor(renderer1, 255, 0, 0, 255);
                    // diviser limage
                    SDL_RenderDrawLine(renderer1,266,0,532,600);
                    SDL_RenderDrawLine(renderer1,0,300,800,300);
                    SDL_RenderDrawLine(renderer1,532,0,266,600);


                    // les murs
                    SDL_RenderDrawLine(renderer1,268,5,530,5);
                    SDL_RenderDrawLine(renderer1,800,300,532,600);
                    SDL_RenderDrawLine(renderer1,335,450,466,450);

                    //le triangle 
                    SDL_RenderDrawLine(renderer1,320,290,320,309);
                    SDL_RenderDrawLine(renderer1,300,299,320,290);
                    SDL_RenderDrawLine(renderer1,300,299,320,309);



                    a= SDL_InitSubSystem(SDL_INIT_AUDIO);


                    SDL_LimitFPS(frame);


                    






                    
                    

                    
                    //*************************************
                    SDL_RenderPresent(renderer1);
                    //*************************************

                    SDL_bool program_lauched =SDL_TRUE;

                    while(program_lauched){
                        SDL_Event event;

                        while(SDL_PollEvent(&event)){
                            switch (event.type)
                            {
                                 case SDL_QUIT:
                                program_lauched= SDL_FALSE;
                                SDL_DestroyRenderer(renderer1);
                        SDL_DestroyWindow(window);

                                    break;

                                case SDL_KEYDOWN:
                                    switch (event.key.keysym.sym)
                                    {
                                        case SDLK_LEFT:
                                    printf(" à gauche\n");
                                    program_lauched= SDL_TRUE;
                                        break;

                                    case SDLK_RIGHT:
                                    printf(" à droite\n");
                                    program_lauched= SDL_TRUE;
                                        break;
                                    
                                    default:
                                        break;
                                    }
                                    
                                    default:
                                 
                                 
                                 break;
                            }
                        }

                    }
                    printf("on quitte\n");

                    frame=SDL_GetTicks()+16;
                    


                    SDL_RenderClear(renderer1);
                    SDL_DestroyRenderer(renderer1);
                    SDL_DestroyWindow(window);






                    SDL_QuitSubSystem( SDL_INIT_AUDIO );
                    SDL_Quit();
                    application->state = APPLICATION_STATE_QUIT;
            
                break;
            case APPLICATION_STATE_QUIT:
                break;
        }
    }
}

void Application_shutDown(struct Application *application) {
    SDL_DestroyRenderer(application->renderer);
    SDL_DestroyWindow(application->window);
    Menu_delete(application->menu);
    free(application);
    application = NULL;
    IMG_Quit();
    SDL_Quit();
}
