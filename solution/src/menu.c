#include "menu.h"
#include "spritesheet.h"
#include "sdl2.h"

#include <stdbool.h>

struct Menu *Menu_initialize(SDL_Renderer *renderer) {
    struct Menu *menu;
    menu = malloc(sizeof(struct Menu));
    menu->renderer = renderer;
    menu->state = MENU_PLAY_FOCUS;
    menu->background = Spritesheet_create(BACKGROUND_FILENAME, 1, 1, 1, renderer);
    menu->title = Spritesheet_create(TITLE_FILENAME, 1, 1, 1, renderer);
    menu->play = Spritesheet_create(PLAY_FILENAME, 1, 1, 1, renderer);
    menu->quit = Spritesheet_create(QUIT_FILENAME, 1, 1, 1, renderer);
    menu->difficulty = Spritesheet_create(DIFFICULTY_FILENAME, 1, 1, 1, renderer);
    menu->easy = Spritesheet_create(EASY_FILENAME, 1, 1, 1, renderer);
    menu->medium = Spritesheet_create(MEDIUM_FILENAME, 1, 1, 1, renderer);
    menu->hard = Spritesheet_create(HARD_FILENAME, 1, 1, 1, renderer);
    return menu;
}

int Menu_alpha(bool chosen) {
    return chosen ? 255 : 64;
}

void Menu_run(struct Menu *menu) {
    SDL_Event e;
    
    while (menu->state != MENU_PLAY && menu->state != MENU_QUIT) {
        
        while (SDL_PollEvent(&e) != 0) {
            if (e.type == SDL_QUIT) {
                menu->state = MENU_QUIT;
            } else if (e.type == SDL_KEYDOWN) {
                switch (e.key.keysym.sym) {
                    
                    case SDLK_LEFT:
                        if(menu->state == MENU_PLAY_FOCUS){
                            menu->state = MENU_QUIT_FOCUS;
                        }else if(menu->state == MENU_QUIT_FOCUS){
                            menu->state = MENU_PLAY_FOCUS;
                        }else if(menu->state == MENU_EASY_FOCUS){
                            menu->state = MENU_HARD_FOCUS;
                        }else if(menu->state == MENU_HARD_FOCUS){
                            menu->state = MENU_MEDIUM_FOCUS;
                        }else if(menu->state == MENU_MEDIUM_FOCUS){
                            menu->state = MENU_EASY_FOCUS;
                        }
                        break;

                    case SDLK_RIGHT:
                        if(menu->state == MENU_PLAY_FOCUS){
                            menu->state = MENU_QUIT_FOCUS;
                        }else if(menu->state == MENU_QUIT_FOCUS){
                            menu->state = MENU_PLAY_FOCUS;
                        }else if(menu->state == MENU_EASY_FOCUS){
                            menu->state = MENU_MEDIUM_FOCUS;
                        }else if(menu->state == MENU_MEDIUM_FOCUS){
                            menu->state = MENU_HARD_FOCUS;
                        }else if(menu->state == MENU_HARD_FOCUS){
                            menu->state = MENU_EASY_FOCUS;
                        }
                        break;

                    case SDLK_DOWN:
                        if(menu->state == MENU_PLAY_FOCUS){
                            menu->state = MENU_EASY_FOCUS; 
                        }else if (menu->state == MENU_QUIT_FOCUS){
                            menu->state = MENU_HARD_FOCUS; 
                        }//else if(menu->state == MENU_HARD_FOCUS){
                               // menu->state = MENU_DIFFICULTY_FOCUS ;
                        //}
                        //else if(menu->state == MENU_MEDIUM_FOCUS){
                                //menu->state = MENU_DIFFICULTY_FOCUS ;
                        //}
                        //else if(menu->state == MENU_EASY_FOCUS){
                               // menu->state = MENU_DIFFICULTY_FOCUS ;
                        //}
                        break;   

                    case SDLK_UP:
                        if (menu->state == MENU_EASY_FOCUS || menu->state == MENU_MEDIUM_FOCUS){
                            menu->state = MENU_PLAY_FOCUS;
                        }else if (menu->state == MENU_HARD_FOCUS){
                            menu->state = MENU_QUIT_FOCUS;
                        }else if(menu->state == MENU_DIFFICULTY_FOCUS){
                                menu->state = MENU_EASY_FOCUS ;
                        }
                        break;    
                        
                    case SDLK_RETURN:
                        if (menu->state == MENU_PLAY_FOCUS) {
                            menu->state = MENU_PLAY;
                        } else if (menu->state == MENU_QUIT_FOCUS) {
                            menu->state = MENU_QUIT;
                        }else if(menu->state == MENU_DIFFICULTY_FOCUS){
                            printf("dicifflie\n");
                        }else if(menu->state == MENU_EASY_FOCUS){
                            printf("Easy\n");
                        }else if(menu->state == MENU_HARD_FOCUS){
                            printf("Hard\n");
                        }else if(menu->state == MENU_MEDIUM_FOCUS){
                            printf("Medium\n");
                        }

                        break;
                }
            }
        }
        SDL_SetRenderDrawColor(menu->renderer, 0x00, 0x00, 0x00, 0x00 );
        SDL_RenderClear(menu->renderer);
        Spritesheet_render(menu->background, BACKGROUND_X, BACKGROUND_Y,
                           255, 0);
        Spritesheet_render(menu->title,      TITLE_X,      TITLE_Y,
                           255, 0);
        Spritesheet_render(menu->play,       PLAY_X,       PLAY_Y,
                           Menu_alpha(menu->state == MENU_PLAY_FOCUS), 0);
        Spritesheet_render(menu->quit,       QUIT_X,       QUIT_Y,
                           Menu_alpha(menu->state == MENU_QUIT_FOCUS), 0);
        Spritesheet_render(menu->difficulty, DIFFICULTY_X,       DIFFICULTY_Y,
                           Menu_alpha(menu->state == MENU_DIFFICULTY_FOCUS), 0);
        Spritesheet_render(menu->easy, EASY_X,       EASY_Y,
                           Menu_alpha(menu->state == MENU_EASY_FOCUS), 0);
        Spritesheet_render(menu->medium, MEDIUM_X,       MEDIUM_Y,
                           Menu_alpha(menu->state == MENU_MEDIUM_FOCUS), 0);
        Spritesheet_render(menu->hard, HARD_X,       HARD_Y,
                           Menu_alpha(menu->state == MENU_HARD_FOCUS), 0);
        SDL_RenderPresent(menu->renderer);
    }
}

void Menu_delete(struct Menu *menu) {
    if (menu != NULL) {
        Spritesheet_delete(menu->background);
        Spritesheet_delete(menu->title);
        Spritesheet_delete(menu->play);
        Spritesheet_delete(menu->quit);
        Spritesheet_delete(menu->difficulty);
        Spritesheet_delete(menu->easy);
        Spritesheet_delete(menu->medium);
        Spritesheet_delete(menu->hard);
        free(menu);
    }
}
